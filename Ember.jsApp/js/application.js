﻿var app = Ember.Application.create();

app.Router.map(function() {
    this.route('about');
    this.route('groups');
    this.resource('devs');
    this.resource('dev', { path: '/devs/:name' });
});

app.AboutController = Ember.Controller.extend({
    name: "Jordan"
});

app.GroupsRoute = Ember.Route.extend({
    model: function () {
        return app.GROUPS;
    }
});

app.DevsRoute = Ember.Route.extend({
    model: function () {
        return app.DEVS;
    }
});

app.DevRoute = Ember.Route.extend({
    model: function (params) {
        return app.DEVS.findBy('name', params.name);
    }
});

app.GROUPS = [
    {
        name: "Qualtrax",
        logo: "resources/qualtrax.jpg",
        about: "Qualtrax Devs"
    }
];

app.DEVS = [
    {
        name: "Jordan Ramey",
        picture: "resources/me.jpg"
    }
];